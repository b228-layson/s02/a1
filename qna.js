/* 

Quiz
What is the term given to unorganized code that's very hard to work with?
spaghetti Code

How are object literals written in JS?
let person = {
    fname: "Wanda",
    lname: "Maximoff",
    age: 18
};


What do you call the concept of organizing information and functionality to belong to an object?
Object Oriented  Programming

If the studentOne object has a method named enroll(), how would you invoke it?
console.log(studentOne.enroll());

True or False: Objects can have objects as properties.
True

What is the syntax in creating key-value pairs?

it is:

fname: "Wanda"

in:

let person = {
    fname: "Wanda",
    lname: "Maximoff",
    age: 18
};

True or False: A method can have no parameters and still work.
True

True or False: Arrays can have objects as elements.
True

True or False: Arrays are objects.
True

True or False: Objects can have arrays as properties.
True

*/